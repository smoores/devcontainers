(* [make_output_dir_path ()] returns a string representing the absolute
   filepath that the devcontainer.json file should be stored in. *)
let make_output_dir_path () = Unix.getcwd () ^ "/.devcontainer"

let is_home_var varstring = String.sub varstring 0 5 = "HOME="

(* [make_devcontainer_template_path "ocaml"] returns a string representing the
   absolute filepath to the template file for the [ocaml] devcontainer. *)
let make_devcontainer_template_path devcontainer_name =
  let environment_list = Array.to_list (Unix.environment ()) in
  let home_var =
    try List.find is_home_var environment_list with
    | Not_found -> failwith "Can't find user's home directory"
  in
  String.sub home_var 5 (String.length home_var - 5)
  ^ "/templates/"
  ^ devcontainer_name
  ^ ".json"

(* [safe_mkdir "/some/path"] creates a directory at "/some/path" if
   no file currently exists at that location. Otherwise does nothing. *)
let safe_mkdir path = if Sys.file_exists path then () else Unix.mkdir path 777

(* [devcontainer_template_exists "ocaml"] returns [true] if a devcontainer template
   is installed for [ocaml], otherwise returns [false]. *)
let devcontainer_template_exists devcontainer_name =
  Sys.file_exists (make_devcontainer_template_path devcontainer_name)

(* [read_devcontainer_template "ocaml"] returns a [Bytes.t] with the contents of the
   [ocaml] devcontainer template file. *)
let read_devcontainer_template devcontainer_name =
  let devcontainer_path = make_devcontainer_template_path devcontainer_name in
  let devcontainer_file_descriptor =
    Unix.openfile
      (make_devcontainer_template_path devcontainer_name)
      [ Unix.O_RDONLY ] 644
  in
  let devcontainer_stats = UnixLabels.stat devcontainer_path in
  let devcontainer_contents = Bytes.create devcontainer_stats.st_size in
  let (_ : int) =
    Unix.read devcontainer_file_descriptor devcontainer_contents 0
      (Bytes.length devcontainer_contents)
  in
  devcontainer_contents

(* [write_devcontainer my_bytes_buffer] writes the contents of [my_bytes_buffer]
   to the filepath ["$(pwd)/.devcontainer/devcontainer.json"] *)
let write_devcontainer devcontainer_contents =
  let output_dir_path = make_output_dir_path () in
  safe_mkdir output_dir_path;
  let output_path = output_dir_path ^ "/devcontainer.json" in
  let output_file_descriptor =
    Unix.openfile output_path [ Unix.O_WRONLY; Unix.O_CREAT ] 644
  in
  let (_ : int) =
    Unix.write output_file_descriptor devcontainer_contents 0
      (Bytes.length devcontainer_contents)
  in
  ()

(* [copy_devcontainer "ocaml"] copies the contents of the [ocaml] devcontainer
   template to the filepath ["$(pwd)/.devcontainer/devcontainer.json"] *)
let copy_devcontainer devcontainer_name =
  let file_contents = read_devcontainer_template devcontainer_name in
  write_devcontainer file_contents

(* [install_devcontainer "ocaml"] copies the contents of the [ocaml] devcontainer
   template to the filepath ["$(pwd)/.devcontainer/devcontainer.json"] if it exists,
   or prints a message notifying the user that the template doesn't exist. *)
let install_devcontainer devcontainer_name =
  if devcontainer_template_exists devcontainer_name
  then copy_devcontainer devcontainer_name
  else failwith ("The container " ^ devcontainer_name ^ " doesn't exist")

let container_param =
  let open Core.Command.Param in
  anon ("container" %: string)

let command =
  Core.Command.basic ~summary:"Install a devcontainer in the current directory"
    ~readme:(fun () -> "Later")
    (Core.Command.Param.map container_param ~f:(fun devcontainer_name () ->
         install_devcontainer devcontainer_name))

let () = Core.Command.run ~version:"1.0" ~build_info:"RWO" command
