FROM debian

# Install opam
RUN apt-get update
RUN apt-get install -y opam

# Initialize opam with the auto flag. Disable sandboxing
# to skip unnecessary bubblewrap (since this is in a container).
RUN opam init -a --disable-sandboxing

# opam installs an old version of ocaml by default;
# switch to the latest available in debian instead
RUN opam switch create latest 4.12.0

# Manually set the environment variables set by
# eval `opam config env` so that they're available
# when the container starts.
ENV OPAM_SWITCH_PREFIX=/root/.opam/latest
ENV CAML_LD_LIBRARY_PATH='/root/.opam/latest/lib/stublibs:Updated by package ocaml'
ENV OCAML_TOPLEVEL_PATH=/root/.opam/latest/lib/toplevel
ENV MANPATH=:/root/.opam/latest/man
ENV PATH=/root/.opam/latest/bin:$PATH

RUN opam update
# Install utop and merlin for development
RUN opam install -y utop
RUN opam install -y merlin
RUN opam install -y ounit2
RUN opam install -y ocamlformat
RUN opam install -y core
RUN opam install -y cohttp-lwt-unix